<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\View\View;
use Livewire\Component;
use Illuminate\Support\Facades\Http;
use Livewire\WithPagination;

class MoviesDashboard extends Component
{
    use WithPagination;

    protected $queryString = [
        'page' => ['except' => 1]
    ];

    public $search;

    public function updatedSearch(): void
    {
        $this->resetPage();
    }

    public function paginationView()
    {
        return 'livewire.tailwind-pagination';
    }

    /**
     * Returns a raw array from the movie DB database.
     * Automatically provides an api key.
     *
     * @param string $endpoint
     * @param array $params
     * @return array
     */
    public function apiQuery(string $endpoint, array $params = []): array
    {
        return Http::withOptions([
            // My local environment crashes Guzzle with an SSL error unless verify is disabled.
            'verify' => false
        ])->get(config('services.moviedbapi.url') . $endpoint,
            array_merge(['api_key' => config('services.moviedbapi.key')], $params))->json();
    }

    /**
     * Returns the collection of the processed movie results from the moviedb API.
     *
     * @return Collection
     */
    public function getMoviesQueryProperty(): Collection
    {
        $genres = collect($this->apiQuery('/genre/movie/list')['genres']);

        // If no search is provided - look the trending movies up.
        if ($this->search == '') {
            $items = $this->apiQuery('/trending/movie/day', ['page' => $this->page]);
        } else {
            // Otherwise - search for provided search input.
            $items = $this->apiQuery('/search/movie', [
                'query' => $this->search,
                'page' => $this->page
            ]);
        }

        $items = collect($items);

        $items['results'] = collect($items['results'])->transform(function ($item) use ($genres) {
            // Add a year attribute to each item array.
            $item['release_year'] = array_key_exists('release_date', $item) ? Carbon::parse($item['release_date'])->year : null;
            // Add the genres to each item.
            $item['genres'] = $genres->whereIn('id', $item['genre_ids'])->pluck('name')->toArray();
            return $item;
        });

        return $items;
    }

    /**
     * Generate the paginator for the query results and render the datatable.
     *
     * @return View
     */
    public function render(): View
    {
        $items = $this->moviesQuery;

        $per_page = array_key_exists('results', $items->toArray()) ? count($items['results']) : 0;

        $paginator = array_key_exists('results', $items->toArray()) && $items['total_results'] > 0
            ? new LengthAwarePaginator($items['results'], $items['total_results'], $per_page, $this->page)
            : new LengthAwarePaginator([], 0, 1, 1);

        return view('livewire.movies-dashboard', [
            'movies' => $paginator
        ]);
    }
}
