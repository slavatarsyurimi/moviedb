{{-- Disclaimer: this component is kinda 'stolen' from Tainwind UI via the livewire demo github course
    Since this is a non-commercial demo project it doesn't matter much imo. Gonna probably but Tailwind UI
   in the future anyway --}}
@props([
    'nopadding' => false
])

@php
    $classes = ($nopadding ? '' : 'px-6 py-2 ') . 'whitespace-no-wrap text-sm leading-5 text-cool-gray-900';
@endphp

<td {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</td>
