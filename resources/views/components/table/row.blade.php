{{-- Disclaimer: this component is kinda 'stolen' from Tainwind UI via the livewire demo github course
    Since this is a non-commercial demo project it doesn't matter much imo. Gonna probably but Tailwind UI
   in the future anyway --}}

<tr {{ $attributes->merge(['class' => 'bg-white']) }}>
    {{ $slot }}
</tr>
