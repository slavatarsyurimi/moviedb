{{-- Disclaimer: this component is kinda 'stolen' from Tainwind UI via the livewire demo github course
    Since this is a non-commercial demo project it doesn't matter much imo. Gonna probably but Tailwind UI
   in the future anyway --}}

<div class="align-middle min-w-full overflow-x-auto shadow overflow-hidden sm:rounded-lg">
    <table class="min-w-full divide-y divide-cool-gray-200">
        <thead>
        <tr>
            {{ $head }}
        </tr>
        </thead>

        <tbody {{ $attributes->merge(['class' => 'bg-white divide-y divide-cool-gray-200']) }}>
            {{ $body }}
        </tbody>
    </table>
</div>
