<input type="text" {{ $attributes->except(['type', 'class']) }}
    {{ $attributes->merge(['class' => 'mt-1 focus:ring-indigo-500 border-2 p-4 focus:border-indigo-500 block w-full h-12 shadow-sm sm:text-sm border-gray-200 rounded-md']) }}>
