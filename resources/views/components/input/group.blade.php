@props([
    'label' => null
])

<div class="col-span-6">
    @if($label)
        <label for="street_address" class="block text-sm font-medium text-gray-700">{{ $label }}</label>
    @endif

    {{ $slot }}
</div>
