<div>
    {{-- Search movie --}}
    <x-input.group label="Search a movie">
        <x-input.text wire:model="search" name="search" />
    </x-input.group>

    <div>
        @if($search == '')
            <div class="mt-3 p-3 border-1 rounded-md text-center bg-green-400 text-white">
                Search for a movie by typing text in the input field above.
                The trending movies of today are currently shown.
            </div>
        @endif
    </div>

    {{-- Results movie table --}}
    <div class="mt-6">
        <x-table wire:loading.class="opacity-75">
            <x-slot name="head">
                <x-table.heading class="text-left"></x-table.heading>
                <x-table.heading class="text-left">Title</x-table.heading>
                <x-table.heading class="text-left">Year</x-table.heading>
                <x-table.heading class="text-left">Genres</x-table.heading>
                <x-table.heading class="text-left">Rank</x-table.heading>
            </x-slot>

            <x-slot name="body">
                @forelse($movies as $movie)
                    <x-table.row wire:key="{{ $movie['id'] }}">
                        <x-table.cell>
                            @if($movie['backdrop_path'] != '')
                                <img class="img-thumbnail rounded-md"
                                     src="{{ config('services.moviedbapi.img_path') . $movie['backdrop_path'] }}" alt="">
                            @else
                                <div style="width: 80px; height: 45px"></div>
                            @endif
                        </x-table.cell>

                        <x-table.cell>
                            {{ $movie['title'] }}
                        </x-table.cell>

                        <x-table.cell>
                            {{ $movie['release_year'] }}
                        </x-table.cell>

                        <x-table.cell>
                            <div class="flex flex-col space-y-2 lg:block">
                                @forelse($movie['genres'] as $genre)
                                    <span class="bg-gray-400 text-white mr-2 px-2.5 py-1.5 rounded-md">{{ $genre }}</span>
                                @empty
                                    Geen genres gevonden
                                @endforelse
                            </div>
                        </x-table.cell>

                        <x-table.cell>
                            {{ $movie['vote_average'] }}
                        </x-table.cell>
                    </x-table.row>
                @empty
                    <x-table.row>
                        <x-table.cell :nopadding="true" colspan="5">
                            <div class="w-full text-center px-3 py-5">
                                No movies found
                            </div>
                        </x-table.cell>
                    </x-table.row>
                @endforelse
            </x-slot>
        </x-table>

        <div class="mt-6">
            {{ $movies->links() }}
        </div>
    </div>
</div>
