<x-layout :livewire="true">
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Movies') }}
        </h2>
    </x-slot>

    <x-section>
        <livewire:movies-dashboard />

        <style>
            .img-thumbnail {
                max-width: 80px;
            }

            @media screen and (max-width: 1023px){
                .img-thumbnail {
                    max-width: 150px;
                }
            }
        </style>
    </x-section>
</x-layout>
